from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.http import HttpRequest
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.utils import timezone

from app.forms import ContactForm
from app.models import Article, Config, Social, Module, UserProfile, Sitemap


# TODO Search
# TODO image picker
# Create your views here.

def home(request):
    """Renders the home page."""
    website = get_object_or_404(Config, pk=1)
    article_object = Article.objects.all()
    article_list = article_object.filter(published_date__lte=timezone.now()).order_by('-published_date')
    paginator = Paginator(article_list, 6)
    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        articles = paginator.page('1')
    except EmptyPage:
        articles = paginator.page(paginator.num_pages)

    assert isinstance(request, HttpRequest)
    return render(
        request,
        'index.html',
        {
            'title': 'Home',
            'meta': website.meta,
            'articles': articles,
        }
    )


def news(request):
    """Renders the news page."""
    website = get_object_or_404(Config, pk=1)
    article_object = Article.objects.all()
    article_list = article_object.filter(published_date__lte=timezone.now(), topic='NEWS').order_by('-published_date')
    paginator = Paginator(article_list, 6)
    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        articles = paginator.page('1')
    except EmptyPage:
        articles = paginator.page(paginator.num_pages)

    assert isinstance(request, HttpRequest)
    return render(
        request,
        'index.html',
        {
            'title': 'News',
            'meta': website.meta,
            'articles': articles,
        }
    )


def politics(request):
    """Renders the politics page."""
    website = get_object_or_404(Config, pk=1)
    article_object = Article.objects.all()
    article_list = article_object.filter(published_date__lte=timezone.now(), topic='POLITICS').order_by(
        '-published_date')
    paginator = Paginator(article_list, 6)
    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        articles = paginator.page('1')
    except EmptyPage:
        articles = paginator.page(paginator.num_pages)

    assert isinstance(request, HttpRequest)
    return render(
        request,
        'index.html',
        {
            'title': 'Politics',
            'meta': website.meta,
            'articles': articles,
        }
    )


def technology(request):
    """Renders the technology page."""
    website = get_object_or_404(Config, pk=1)
    article_object = Article.objects.all()
    article_list = article_object.filter(published_date__lte=timezone.now(), topic='TECHNOLOGY').order_by(
        '-published_date')
    paginator = Paginator(article_list, 6)
    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        articles = paginator.page('1')
    except EmptyPage:
        articles = paginator.page(paginator.num_pages)

    assert isinstance(request, HttpRequest)
    return render(
        request,
        'index.html',
        {
            'title': 'Technology',
            'meta': website.meta,
            'articles': articles,
        }
    )


def entertainment(request):
    """Renders the entertainment page."""
    website = get_object_or_404(Config, pk=1)
    article_object = Article.objects.all()
    article_list = article_object.filter(published_date__lte=timezone.now(), topic='ENTERTAINMENT').order_by(
        '-published_date')
    paginator = Paginator(article_list, 6)
    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        articles = paginator.page('1')
    except EmptyPage:
        articles = paginator.page(paginator.num_pages)

    assert isinstance(request, HttpRequest)
    return render(
        request,
        'index.html',
        {
            'title': 'Entertainment',
            'meta': website.meta,
            'articles': articles,
        }
    )


def trends(request):
    """Renders the trends page."""
    website = get_object_or_404(Config, pk=1)
    article_object = Article.objects.all()

    article_list = article_object.filter(published_date__lte=timezone.now(), topic='TRENDS').order_by(
        '-published_date')
    paginator = Paginator(article_list, 6)
    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        articles = paginator.page('1')
    except EmptyPage:
        articles = paginator.page(paginator.num_pages)

    assert isinstance(request, HttpRequest)
    return render(
        request,
        'index.html',
        {
            'title': 'Trends',
            'meta': website.meta,
            'articles': articles,
        }
    )


def article_view(request, topic, slug):
    """Renders the article."""

    article = get_object_or_404(Article, slug=slug)
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'article.html',
        {
            'title': article.title,
            'meta': article.meta,
            'article': article,
        }
    )


def profile(request, pk):
    """Renders the User Profile page."""
    website = get_object_or_404(Config, pk=1)
    article_object = Article.objects.all()
    user = get_object_or_404(UserProfile, pk=pk)
    article_list = article_object.filter(published_date__lte=timezone.now(), author=user.username).order_by(
        '-published_date')
    paginator = Paginator(article_list, 6)
    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        articles = paginator.page('1')
    except EmptyPage:
        articles = paginator.page(paginator.num_pages)

    assert isinstance(request, HttpRequest)
    return render(
        request,
        'profile.html',
        {
            'title': user.username.get_full_name,
            'meta': website.meta,
            'profile': user,
            'articles': articles,
        }
    )


def about_view(request):
    """Renders the About Us page."""
    website = get_object_or_404(Config, pk=1)
    userprofiles = UserProfile.objects.filter(active=True).order_by('pk')
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'about.html',
        {
            'title': 'About Us',
            'meta': website.meta,
            'userprofiles': userprofiles,
        }
    )


def contact_us(request):
    """Renders the Contact Us form."""
    website = get_object_or_404(Config, pk=1)
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            contact = form.save(commit=False)
            contact.contact_date = timezone.now()
            contact.save()
            return HttpResponseRedirect(reverse('home'))
        else:
            return HttpResponseRedirect(reverse('contact_us'))
    else:
        form = ContactForm()
        assert isinstance(request, HttpRequest)
        return render(
            request,
            'contact.html',
            {
                'title': 'Contact Us',
                'meta': website.meta,
                'form': form,
            }
        )


def sitemap_view(request):
    sitemap = get_object_or_404(Sitemap, pk=1)
    assert isinstance(request, HttpRequest)
    return render(request, 'sitemap.xml', {'sitemap': sitemap, })
