from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from app.models import CustomUser
from app.models import Contact


class CustomUserCreationForm(UserCreationForm):
    """
    A form that creates a user, with no privileges, from the given email and
    password.
    """

    def __init__(self, *args, **kargs):
        super(CustomUserCreationForm, self).__init__(*args, **kargs)

    class Meta:
        model = CustomUser
        fields = ("username", "email", "first_name", "last_name",)


class CustomUserChangeForm(UserChangeForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """

    def __init__(self, *args, **kargs):
        super(CustomUserChangeForm, self).__init__(*args, **kargs)

    class Meta:
        model = CustomUser
        fields = ("username", "email", "first_name", "last_name",)


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ('name', 'email', 'message',)

    name = forms.CharField(required=True,
                           max_length=140,
                           widget=forms.TextInput({
                               'id': "demo-name",
                               'name': "demo-name",
                               'placeholder': 'Name, eg: John Smith (required)'}))
    email = forms.EmailField(required=True,
                             widget=forms.EmailInput({
                                 'id': "demo-email",
                                 'name': "demo-email",
                                 'placeholder': 'Email, eg: someone@example.com (required)'}))

    message = forms.CharField(required=False,
                              widget=forms.Textarea({
                                  'rows': "6",
                                  'id': "demo-message",
                                  'name': "demo-message",
                                  'placeholder': 'Message'}))
