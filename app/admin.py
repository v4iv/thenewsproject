from django.contrib import admin

# Register your models here.
from django.contrib.auth.admin import UserAdmin

from app.forms import CustomUserChangeForm
from app.forms import CustomUserCreationForm
from app.models import CustomUser, Contact, Article, ArticleAdmin, UserProfile, Social, Config, Module, Sitemap, Image
from django.utils.translation import ugettext_lazy as _


class CustomUserAdmin(UserAdmin):
    # The forms to add and change user instances

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference the removed 'username' field

    form = CustomUserChangeForm
    add_form = CustomUserCreationForm

    fieldsets = ((_('User'), {'fields': ('username', 'email', 'password')}),
                 (_('Personal Info'), {'fields': ('first_name', 'last_name', 'date_of_birth', 'sex')}),
                 (_('Permissions'),
                  {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
                 (_('Important Dates'), {'fields': ('last_login', 'date_joined')}),
                 )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')
        }),
    )
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff')
    search_fields = ('username', 'email', 'first_name', 'last_name')
    ordering = ('username',)


admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(Config)
admin.site.register(Article, ArticleAdmin)
admin.site.register(UserProfile)
admin.site.register(Contact)
admin.site.register(Social)
admin.site.register(Module)
admin.site.register(Sitemap)
admin.site.register(Image)
