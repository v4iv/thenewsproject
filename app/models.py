from django.contrib import admin
from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.mail import send_mail
from django.db import models
from django.db.models.signals import post_save
from django.utils import timezone
from django.utils.http import urlquote
from django.utils.translation import ugettext_lazy as _
from tinymce.models import HTMLField

from thenewsproject.settings import AUTH_USER_MODEL


class CustomUserManager(BaseUserManager):
    def _create_user(self, username, first_name, last_name, email, password, is_staff, is_superuser,
                     **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """

        now = timezone.now()

        if not email:
            raise ValueError('Email is Required!')
        if not username:
            raise ValueError('Username is Required!')

        email = self.normalize_email(email)
        user = self.model(username=username, first_name=first_name, last_name=last_name, email=email,
                          is_staff=is_staff, is_active=True, is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, first_name, last_name, email, password=None, **extra_fields):
        return self._create_user(username, first_name, last_name, email, password, False, False, **extra_fields)

    def create_superuser(self, username, first_name, last_name, email, password, **extra_fields):
        return self._create_user(username, first_name, last_name, email, password, True, True, **extra_fields)


class CustomUser(AbstractBaseUser, PermissionsMixin):
    """
    A Custom User model with admin-compliant Permissions.

    First Name, Last Name and Email are required.
    """
    username = models.CharField(_('Username'), max_length=15, unique=True,
                                help_text="Required. 15 characters or fewer. Letters, digits and @/./+/-/_ only.")
    email = models.EmailField(_('Email Address'), max_length=254, unique=True)
    first_name = models.CharField(_('First Name'), max_length=30)
    last_name = models.CharField(_('Last Name'), max_length=30)
    date_of_birth = models.DateField(_('Date of Birth'), blank=True, null=True)
    MALE = 'M'
    FEMALE = 'F'
    UNKNOWN = 'U'
    SEX_CHOICES = (
        (UNKNOWN, 'Do Not Wish To Disclose'),
        (MALE, 'Male'),
        (FEMALE, 'Female'),
    )
    sex = models.CharField(_('Sex (M/F)'), max_length=1, choices=SEX_CHOICES, default=UNKNOWN)
    is_staff = models.BooleanField(_('Staff Status'), default=False,
                                   help_text=_('Designates whether the user can log into this admin site.'))
    is_active = models.BooleanField(_('Active'), default=True,
                                    help_text='Designates whether this user should be treated as'
                                              'active. Unselect this instead of deleting accounts')
    date_joined = models.DateTimeField(_('Date Joined'), default=timezone.now)
    objects = CustomUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'email']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_absolute_url(self):
        return "/users/%s/" % urlquote(self.username)

    def get_full_name(self):
        """
        Full Name.
        :return: Returns the first_name plus the last_name, with space in between.
        """
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """
        Short Name.
        :return: Returns the short name for the user.
        """
        return self.first_name

    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        :param subject: Subject of the mail.
        :param message:  Message content of the mail.
        :param from_email: the senders email.
        """
        send_mail(subject, message, from_email, [self.email])


class Config(models.Model):
    title = models.CharField(max_length=30)
    logo = models.ImageField(upload_to='uploads/', default='uploads/logo.jpg')
    quote = models.CharField(max_length=200, blank=True, null=True)
    about = models.TextField()
    meta = models.TextField(blank=True, null=True)
    analytics_code = models.CharField(max_length=20, default="XX-XXXXXXXX-X")
    last_updated = models.DateTimeField(default=timezone.now)

    def publish(self):
        self.last_updated = timezone.now()
        self.save()

    def __str__(self):
        return self.title


class Module(models.Model):
    website = models.URLField()
    google_analytics_module = models.BooleanField(default=False)
    featured_module = models.BooleanField(default=True)
    quote_module = models.BooleanField(default=True)
    news_module = models.BooleanField(default=True)
    politics_module = models.BooleanField(default=True)
    technology_module = models.BooleanField(default=True)
    entertainment_module = models.BooleanField(default=True)
    trends_module = models.BooleanField(default=True)
    top_news_module = models.BooleanField(default=True)
    top_politics_module = models.BooleanField(default=True)
    top_technology_module = models.BooleanField(default=True)
    top_entertainment_module = models.BooleanField(default=True)
    top_trends_module = models.BooleanField(default=True)
    about_module = models.BooleanField(default=True)
    contact_module = models.BooleanField(default=True)
    last_updated = models.DateTimeField(default=timezone.now)

    def publish(self):
        self.last_updated = timezone.now()
        self.save()

    def __str__(self):
        return self.website
        

class Article(models.Model):
    author = models.ForeignKey(AUTH_USER_MODEL)
    title = models.CharField(max_length=140)
    slug = models.SlugField(unique=True)
    subheading = models.CharField(max_length=200, blank=True, null=True)
    NEWS = 'NEWS'
    POLITICS = 'POLITICS'
    TECHNOLOGY = 'TECHNOLOGY'
    ENTERTAINMENT = 'ENTERTAINMENT'
    TRENDS = 'TRENDS'
    CATEGORY = (
        (NEWS, 'News'),
        (POLITICS, 'Politics'),
        (TECHNOLOGY, 'Technology'),
        (ENTERTAINMENT, 'Entertainment'),
        (TRENDS, 'Trends'),
    )
    topic = models.CharField(max_length=15, choices=CATEGORY, default=NEWS)
    image = models.ImageField(upload_to='uploads/', default='uploads/pic05.jpg')
    content = HTMLField()
    featured = models.BooleanField(default=False)
    top = models.BooleanField(default=False)
    meta = models.TextField(blank=True, null=True)
    last_edited = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(default=timezone.now)

    def publish(self):
        self.last_edited = timezone.now()
        self.save()

    def __str__(self):
        return self.title


class ArticleAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}

class Contact(models.Model):
    name = models.CharField(max_length=140)
    email = models.EmailField()
    message = models.TextField(blank=True, null=True)
    contact_date = models.DateTimeField(default=timezone.now)

    def connect(self):
        self.contact_date = timezone.now()
        self.save()

    def __str__(self):
        return self.name


class UserProfile(models.Model):
    username = models.OneToOneField(AUTH_USER_MODEL, on_delete=models.CASCADE, unique=True)
    designation = models.CharField(max_length=30, blank=True, null=True)
    display_picture = models.ImageField(upload_to='uploads/', default='uploads/silhouette.jpg')
    about_me = models.TextField(blank=True, null=True)
    active = models.BooleanField(default=True)
    last_updated = models.DateTimeField(default=timezone.now)

    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            UserProfile.objects.create(username=instance)

    post_save.connect(create_user_profile, sender=AUTH_USER_MODEL)

    def create(self):
        self.save()

    def __str__(self):
        return self.username.get_full_name()


class Social(models.Model):
    icon = models.CharField(max_length=20)
    name = models.CharField(max_length=20)
    link = models.URLField()
    last_updated = models.DateTimeField(default=timezone.now)

    def connect(self):
        self.last_updated = timezone.now()
        self.save()

    def __str__(self):
        return self.name


class Sitemap(models.Model):
    name = models.CharField(max_length=10, default='sitemap')
    sitemap = models.TextField(blank=True, null=True)

    def publish(self):
        self.last_updated = timezone.now()
        self.save()

    def __str__(self):
        return self.name
