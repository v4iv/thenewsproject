from django.http import HttpRequest
from django.shortcuts import get_object_or_404, render
from django.utils import timezone

from app.models import Config, Module, Social, Article


def common_context(request):
    """Renders the Search Result Page"""
    website = get_object_or_404(Config, pk=1)
    module = get_object_or_404(Module, pk=1)
    socials = Social.objects.all()
    article_object = Article.objects.all()
    featured = article_object.filter(featured=True).order_by('-published_date')[:5]
    news_articles = article_object.filter(topic='NEWS', top=True).order_by('-published_date')[:5]
    political_articles = article_object.filter(topic='POLITICS', top=True).order_by('-published_date')[:5]
    technology_articles = article_object.filter(topic='TECHNOLOGY', top=True).order_by('-published_date')[:5]
    entertainment_articles = article_object.filter(topic='ENTERTAINMENT', top=True).order_by('-published_date')[:5]
    trend_articles = article_object.filter(topic='TRENDS', top=True).order_by('-published_date')[:5]

    context = {
        'website': website,
        'module': module,
        'featured': featured,
        'news_articles': news_articles,
        'political_articles': political_articles,
        'technology_articles': technology_articles,
        'entertainment_articles': entertainment_articles,
        'trend_articles': trend_articles,
        'socials': socials,
        'title': 'Search Results',
        'meta': 'Search Results',
        'year': timezone.now().year,
    }

    return context
