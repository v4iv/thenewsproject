from django.conf.urls import url
from django.conf.urls.static import static

from app import views
from thenewsproject import settings

urlpatterns = [
                  url(r'^$', views.home, name='home'),
                  url(r'^news/$', views.news, name='news'),
                  url(r'^politics/$', views.politics, name='politics'),
                  url(r'^technology/$', views.technology, name='technology'),
                  url(r'^entertainment/$', views.entertainment, name='entertainment'),
                  url(r'^trends/$', views.trends, name='trends'),
                  url(r'^(?P<topic>[a-z]+)/article/(?P<slug>[\w-]+)/$', views.article_view, name='article'),
                  url(r'^profile/(?P<pk>[0-9]+)/$', views.profile, name='profile'),
                  url(r'^about/$', views.about_view, name='about'),
                  url(r'^contact/$', views.contact_us, name='contact'),
                  url(r'^sitemap.xml/$', views.sitemap_view, name='sitemap'),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
