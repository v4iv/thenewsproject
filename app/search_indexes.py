from django.utils import timezone
from haystack import indexes
from app.models import Article


class ArticleIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')
    subheading = indexes.CharField(model_attr='subheading')
    author = indexes.CharField(model_attr='author')
    content = indexes.CharField(model_attr='content')
    published_date = indexes.DateTimeField(model_attr='published_date')

    def get_model(self):
        return Article

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(published_date__lte=timezone.now())
